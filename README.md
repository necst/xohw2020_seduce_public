# SeDUCE #

SeDUCE is a project developed for the FPGA Academy project as an entry to the Xilinx Open Hardware 2020 competition.
This project is developed in collaboration with NECSTLab.

### Developers ###

* Giorgio Colomban
* Francesco Tosini
* Kien Tuong Truong